// HAMBURGER  MENU  ICON
(function () {
  var hamburger = {
    navToggle: document.querySelector('.nav-toggle'),
    nav: document.querySelector('nav'),
    doToggle: function (e) {
      e.preventDefault();
      this.navToggle.classList.toggle('expanded');
      this.nav.classList.toggle('expanded');
    }
  };
  hamburger.navToggle.addEventListener('click', function (e) {
    hamburger.doToggle(e);
  });
}());
//================================================
// SLICK  SLIDER  INITILIZATION
$('.testimonialSlider').slick({
  dots: true,
  infinite: false,
  speed: 500,
  slidesToShow: 1,
  slidesToScroll: 1,
  autoplay: true,
  prevArrow: "<a href='javascript:;' class='prev'><img src='assets/images/prev.svg' alt='Previous'></a>",
  nextArrow: "<a href='javascript:;' class='next'><img src='assets/images/next.svg' alt='Previous'></a>",
  responsive: [{
    breakpoint: 992,
    settings: {
      slidesToShow: 1,
      slidesToScroll: 1,
      infinite: false,
      dots: true
    }
  }]
});
if(window.innerWidth < 770){  
  $('.portfolioSlider').slick({
    responsive: [{      
      breakpoint: 770,
      settings: {
        infinite: false,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: false,
        dots: true,
        // prevArrow: "<a href='javascript:;' class='prev'><img src='assets/images/prev.svg' alt='Previous'></a>",
        // nextArrow: "<a href='javascript:;' class='next'><img src='assets/images/next.svg' alt='Previous'></a>",
      }
    }]
  });
}


//  SCROLL  FOR  SECTIONS  AND NAVIGATION
$(document).ready(function () {
  var scrollLink = $('.scroll');
  var nextSec = $('.nextSection');
  // Smooth scrolling
  nextSec.click(function (a) {
    a.preventDefault();
    $('html,body').animate({
      scrollTop: $("#process").offset().top
    }, 100);
  })
  scrollLink.click(function (e) {
    e.preventDefault();
    $('body,html').animate({
      scrollTop: $(this.hash).offset().top
    }, 100);
    console.log($(this.hash));
  });
  // Active link switching
  $(window).scroll(function () {
    // var scrollbarLocation = $(this).scrollTop();

    // scrollLink.each(function() {
    //   var sectionOffset = $(this.hash).offset().top - 65;
    //   if ( sectionOffset <= scrollbarLocation ) {
    //     $(this).parent().addClass('active');
    //     $(this).parent().siblings().removeClass('active');
    //   }
    // })
    // var nav = document.getElementById("navigation");
    // var hght = document.getElementById("home").clientHeight;
    // console.log(hght);
    // if(window.pageYOffset >= hght){
    //   nav.classList.add("posFix");
    //   nav.classList.remove("posStatic");
    // }
    // else{
    //   nav.classList.add("posStatic");
    //   nav.classList.remove("posFix");
    // }
  })

})

function closeNav() {
  $(".nav").removeClass("expanded");
  $(".nav-toggle").removeClass("expanded");
}

// ON  SCROLL   ANIMATION  REVEALS
function onScrollInit(items, trigger) {
  items.each(function () {
    var osElement = $(this),
      osAnimationClass = osElement.attr('data-os-animation'),
      osAnimationDelay = osElement.attr('data-os-animation-delay');
    osElement.css({
      '-webkit-animation-delay': osAnimationDelay,
      '-moz-animation-delay': osAnimationDelay,
      'animation-delay': osAnimationDelay
    });
    var osTrigger = (trigger) ? trigger : osElement;
    osTrigger.waypoint(function () {
      osElement.addClass('animated').addClass(osAnimationClass);
    }, {
      triggerOnce: true,
      offset: '90%'
    });
  });
}
onScrollInit($('.os-animation'));
onScrollInit($('.staggered-animation'), $('.staggered-animation-container'));
//==============================================
// SMOOTH  SCROLL  EFFECTS
// email setting code
$(document).ready(function () {
  $(document).on("click", "#btn_get_a_quote", function () {
    var text_email=$("#Email").val();
    if(text_email.length>0){
    $.ajax({
        method: "POST",
        url: "mail.php",
        data: $("#get_a_quote").serialize()
      })
      .done(function (response) {        
        var data = JSON.parse(response);        
        show_message("show_quote_msg",data.msg);        
      });
    }else{
      alert("Email Require.");
    }

  });
  $(document).on("click", "#btn_contact_us", function () {
    $.ajax({
        method: "POST",
        url: "mail.php",
        data: $("#frmcontactus").serialize()
      })
      .done(function (response) {        
        var data = JSON.parse(response);        
        show_message("show_contact_msg",data.msg);        
      });


  });

  function show_message(div_name,msg) {
    $("#"+div_name).text(msg);
    $("#"+div_name).slideInDown();
    setTimeout(function () {
      $("#"+div_name).slideOut();
    }, 3000);
  }

});

// PARTICLES  BACKGROUND  ANIMATIONS
var canvas = document.getElementById('nokey'),
   can_w = parseInt(canvas.getAttribute('width')),
   can_h = parseInt(canvas.getAttribute('height')),
   ctx = canvas.getContext('2d');
// console.log(typeof can_w);
var ball = {
      x: 0,
      y: 0,
      vx: 0,
      vy: 0,
      r: 0,
      alpha: 1,
      phase: 0
   },
   ball_color = {
       r: 161,
       g: 66,
       b: 88
   },
   R = 2,
   balls = [],
   alpha_f = 0.03,
   alpha_phase = 0,
    
// Line
   link_line_width = 0.8,
   dis_limit = 260,
   add_mouse_point = true,
   mouse_in = false,
   mouse_ball = {
      x: 0,
      y: 0,
      vx: 0,
      vy: 0,
      r: 0,
      type: 'mouse'
   };

// Random speed
function getRandomSpeed(pos){
    var  min = -1,
       max = 1;
    switch(pos){
        case 'top':
            return [randomNumFrom(min, max), randomNumFrom(0.1, max)];
            break;
        case 'right':
            return [randomNumFrom(min, -0.1), randomNumFrom(min, max)];
            break;
        case 'bottom':
            return [randomNumFrom(min, max), randomNumFrom(min, -0.1)];
            break;
        case 'left':
            return [randomNumFrom(0.1, max), randomNumFrom(min, max)];
            break;
        default:
            return;
            break;
    }
}
function randomArrayItem(arr){
    return arr[Math.floor(Math.random() * arr.length)];
}
function randomNumFrom(min, max){
    return Math.random()*(max - min) + min;
}
console.log(randomNumFrom(0, 10));
// Random Ball
function getRandomBall(){
    var pos = randomArrayItem(['top', 'right', 'bottom', 'left']);
    switch(pos){
        case 'top':
            return {
                x: randomSidePos(can_w),
                y: -R,
                vx: getRandomSpeed('top')[0],
                vy: getRandomSpeed('top')[1],
                r: R,
                alpha: 1,
                phase: randomNumFrom(0, 10)
            }
            break;
        case 'right':
            return {
                x: can_w + R,
                y: randomSidePos(can_h),
                vx: getRandomSpeed('right')[0],
                vy: getRandomSpeed('right')[1],
                r: R,
                alpha: 1,
                phase: randomNumFrom(0, 10)
            }
            break;
        case 'bottom':
            return {
                x: randomSidePos(can_w),
                y: can_h + R,
                vx: getRandomSpeed('bottom')[0],
                vy: getRandomSpeed('bottom')[1],
                r: R,
                alpha: 1,
                phase: randomNumFrom(0, 10)
            }
            break;
        case 'left':
            return {
                x: -R,
                y: randomSidePos(can_h),
                vx: getRandomSpeed('left')[0],
                vy: getRandomSpeed('left')[1],
                r: R,
                alpha: 1,
                phase: randomNumFrom(0, 10)
            }
            break;
    }
}
function randomSidePos(length){
    return Math.ceil(Math.random() * length);
}

// Draw Ball
function renderBalls(){
    Array.prototype.forEach.call(balls, function(b){
       if(!b.hasOwnProperty('type')){
           ctx.fillStyle = 'rgba('+ball_color.r+','+ball_color.g+','+ball_color.b+','+b.alpha+')';
           ctx.beginPath();
           ctx.arc(b.x, b.y, R, 0, Math.PI*2, true);
           ctx.closePath();
           ctx.fill();
       }
    });
}

// Update balls
function updateBalls(){
    var new_balls = [];
    Array.prototype.forEach.call(balls, function(b){
        b.x += b.vx;
        b.y += b.vy;
        
        if(b.x > -(50) && b.x < (can_w+50) && b.y > -(50) && b.y < (can_h+50)){
           new_balls.push(b);
        }
        
        // alpha change
        b.phase += alpha_f;
        b.alpha = Math.abs(Math.cos(b.phase));
        // console.log(b.alpha);
    });
    
    balls = new_balls.slice(0);
}

// loop alpha
function loopAlphaInf(){
    
}

// Draw lines
function renderLines(){
    var fraction, alpha;
    for (var i = 0; i < balls.length; i++) {
        for (var j = i + 1; j < balls.length; j++) {
           
           fraction = getDisOf(balls[i], balls[j]) / dis_limit;
            
           if(fraction < 1){
               alpha = (1 - fraction).toString();

               ctx.strokeStyle = 'rgba(0,0,0,'+alpha+')';
               ctx.lineWidth = link_line_width;
               
               ctx.beginPath();
               ctx.moveTo(balls[i].x, balls[i].y);
               ctx.lineTo(balls[j].x, balls[j].y);
               ctx.stroke();
               ctx.closePath();
           }
        }
    }
}

// calculate distance between two points
function getDisOf(b1, b2){
    var  delta_x = Math.abs(b1.x - b2.x),
       delta_y = Math.abs(b1.y - b2.y);    
    return Math.sqrt(delta_x*delta_x + delta_y*delta_y);
}
// add balls if there a little balls
function addBallIfy(){
    if(balls.length < 20){
        balls.push(getRandomBall());
    }
}

// Render
function render(){
    ctx.clearRect(0, 0, can_w, can_h);
    
    renderBalls();
    
    renderLines();
    
    updateBalls();
    
    addBallIfy();
    
    window.requestAnimationFrame(render);
}

// Init Balls
function initBalls(num){
    for(var i = 1; i <= num; i++){
        balls.push({
            x: randomSidePos(can_w),
            y: randomSidePos(can_h),
            vx: getRandomSpeed('top')[0],
            vy: getRandomSpeed('top')[1],
            r: R,
            alpha: 1,
            phase: randomNumFrom(0, 10)
        });
    }
}
// Init Canvas
function initCanvas(){
    canvas.setAttribute('width', window.innerWidth);
    canvas.setAttribute('height', window.innerHeight);
    
    can_w = parseInt(canvas.getAttribute('width'));
    can_h = parseInt(canvas.getAttribute('height'));
}
window.addEventListener('resize', function(e){
    console.log('Window Resize...');
    initCanvas();
});
function goMovie(){
    initCanvas();
    initBalls(30);
    window.requestAnimationFrame(render);
}
goMovie();
// Mouse effect
canvas.addEventListener('mouseenter', function(){
    console.log('mouseenter');
    mouse_in = true;
    balls.push(mouse_ball);
});
canvas.addEventListener('mouseleave', function(){
    console.log('mouseleave');
    mouse_in = false;
    var new_balls = [];
    Array.prototype.forEach.call(balls, function(b){
        if(!b.hasOwnProperty('type')){
            new_balls.push(b);
        }
    });
    balls = new_balls.slice(0);
});
canvas.addEventListener('mousemove', function(e){
    var e = e || window.event;
    mouse_ball.x = e.pageX;
    mouse_ball.y = e.pageY;
    // console.log(mouse_ball);
});