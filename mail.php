<?php
require 'src/Exception.php';
require 'src/PHPMailer.php';
require 'src/SMTP.php';

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

if(isset($_SERVER)){
    if(isset($_REQUEST['formtype'])){      
        sendEmail($_REQUEST,$_REQUEST['formtype']);
        echo json_encode(array("status"=>"200","msg"=>"Successfully sent. We will get back to you very soon!"));
    }
}

function sendEmail($data,$type){
$mail = new PHPMailer(true);  
try {
    //Server settings
    $mail->SMTPDebug = 0;                                 // Enable verbose debug output
    $mail->isSMTP();                                      // Set mailer to use SMTP
    $mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
    $mail->SMTPAuth = true;                               // Enable SMTP authentication
    $mail->Username = 'kinectro.development@gmail.com';                 // SMTP username
    $mail->Password = 'Kin19max';                           // SMTP password
    $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
    $mail->Port = 587;                                    // TCP port to connect to

    //Recipients
    $mail->setFrom('kinectro.web@gmail.com', 'Mailer');
    $mail->addAddress('kinectro.web@gmail.com');     // Add a recipient           
    //Content
    $mail->isHTML(true);        
    if($type==1){
        $subject="Get Quote";
    }else{
        $subject="Contact Us";
    }
    // Set email format to HTML
    $mail->Body    = get_html_body($data,$type);
    $mail->Subject = $subject;
    $mail->send();
    
} catch (Exception $e) {
    echo  json_encode(array("status"=>"210","msg"=>'Message could not be sent. Mailer Error: ', $mail->ErrorInfo));
}

}

function get_html_body($data,$type){
    $body_html="<table>";
        foreach($data as $key=>$value){
            if($key!="formtype"){
            $body_html.="<tr><td>".$key.":</td>";
            $body_html.="<td>".$value."</td><tr>";
            }
        }    
    $body_html.="</table>";
    return $body_html;
}


?>
